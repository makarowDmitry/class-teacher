import DB.ConAndClos;
import DB.EditingDataBase;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import pojo.AttendanceTable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Attendance {
    ConAndClos conAndClos = new ConAndClos();

    private ObservableList<AttendanceTable> table = FXCollections.observableArrayList();

    @FXML
    private TableView<AttendanceTable> tableView;

    @FXML
    private TableColumn<AttendanceTable, Integer> cod;

    @FXML
    private TableColumn<AttendanceTable, String> FIO;

    @FXML
    private TableColumn<AttendanceTable, String> date;

    @FXML
    private TableColumn<AttendanceTable, String> first;

    @FXML
    private TableColumn<AttendanceTable, String> second;

    @FXML
    private TableColumn<AttendanceTable, String> third;

    /**
     * Отображает таблицу "Посещаемость"
     *
     * @throws SQLException - ошибка с SQL кодом и базой данных
     */
    public void displayTable() throws SQLException {
        tableView.getItems().clear();
        conAndClos.connect();
        ResultSet resultSet = conAndClos.stat.executeQuery("SELECT * FROM `посещаемость`");

        ArrayList<Integer> arrCod = new ArrayList<>();
        ArrayList<String> arrFIO = new ArrayList<>();
        ArrayList<String> arrDate = new ArrayList<>();
        ArrayList<String> arrFirst = new ArrayList<>();
        ArrayList<String> arrSecond = new ArrayList<>();
        ArrayList<String> arrThird = new ArrayList<>();
        while (resultSet.next()) {
            arrCod.add(resultSet.getInt(2));
            arrFIO.add(resultSet.getString(3));
            arrDate.add(resultSet.getString(4));
            arrFirst.add(resultSet.getString(5));
            arrSecond.add(resultSet.getString(6));
            arrThird.add(resultSet.getString(7));
        }
        conAndClos.close();

        cod.setCellValueFactory(new PropertyValueFactory<AttendanceTable, Integer>("cod"));
        FIO.setCellValueFactory(new PropertyValueFactory<AttendanceTable, String>("FIO"));
        date.setCellValueFactory(new PropertyValueFactory<AttendanceTable, String>("date"));
        first.setCellValueFactory(new PropertyValueFactory<AttendanceTable, String>("first"));
        second.setCellValueFactory(new PropertyValueFactory<AttendanceTable, String>("second"));
        third.setCellValueFactory(new PropertyValueFactory<AttendanceTable, String>("third"));
        tableView.setItems(table);

        for (int i = 0; i < arrCod.size(); i++) {
            table.add(new AttendanceTable(arrCod.get(i), arrFIO.get(i), arrDate.get(i), arrFirst.get(i), arrSecond.get(i), arrThird.get(i)));
        }

    }

    EditingDataBase editingDataBase = new EditingDataBase();
    Stage primaryStage = new Stage();
    @FXML
    TextField delete;

    /**
     * Удаляет строку из таблицы "Посещаемость"
     *
     * @throws SQLException - ошибка с SQL кодом и базой данных
     */
    public void deleteLineAttendance() throws SQLException {
        editingDataBase.deleteLine("посещаемость", "Код студента", Integer.parseInt(delete.getText()));
    }

    /**
     * Открывает окно редактирования данных в таблице "Посещаемость"
     *
     * @throws Exception
     */
    public void inputMenuEditing() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("tablesediting/attendanceEditing.fxml"));

        primaryStage.setTitle("Редактирование таблицы");
        primaryStage.setScene(new Scene(root, 600, 304));
        primaryStage.show();
    }

    /**
     * Открывает окно добавления строки в таблицу "Посещаемость"
     *
     * @throws Exception
     */
    public void inputMenuAdd() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("adddata/attendance.fxml"));

        primaryStage.setTitle("Добавление данных в таблицу");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }

}
