package DB;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GetData {
    ConAndClos conAndClos = new ConAndClos();

    public String getFIO(String cod) throws SQLException {
        conAndClos.connect();
        ResultSet resultSet = conAndClos.stat.executeQuery("SELECT * FROM `студенты` WHERE `Код студента`= " + cod);
        String FIO = "";
        while (resultSet.next()){
            FIO=resultSet.getString(3);
        }
        conAndClos.close();
        return FIO;
    }
}
