package DB;

import java.sql.SQLException;

public class EditingDataBase {
    ConAndClos conAndClos = new ConAndClos();
    String sql;

    /**
     * Удаление строки из базы данных
     *
     * @param tableName   - название таблицы
     * @param columnName  - название столбца
     * @param valueUnique - значение в строке
     * @throws SQLException - - ошибка с SQL кодом и базой данных
     */
    public void deleteLine(String tableName, String columnName, int valueUnique) throws SQLException {
        conAndClos.connect();
        sql = "DELETE FROM `" + tableName + "` WHERE `" + columnName + "` = " + valueUnique;
        conAndClos.stat.executeUpdate(sql);
        conAndClos.close();
    }

    /**
     * Изменение значения числовых ячеек в строке
     *
     * @param tableName  - название таблицы
     * @param columnName - название столбца
     * @param values     - массив значений(старое и новое)
     * @throws SQLException - - ошибка с SQL кодом и базой данных
     */
    public void updateElementInt(String tableName, String columnName, int[] values) throws SQLException {
        conAndClos.connect();
        sql = "UPDATE `" + tableName + "` SET `" + columnName + "` =" + values[0] + " WHERE `" + tableName + "`." + "`Код студента` = " + values[1];
        conAndClos.stat.executeUpdate(sql);
        conAndClos.close();
    }

    public void updateElementLong(String tableName, String columnName, long[] values) throws SQLException {
        conAndClos.connect();
        sql = "UPDATE `" + tableName + "` SET `" + columnName + "` =" + values[0] + " WHERE `" + tableName + "`." + "`Код студента` = " + values[1];
        conAndClos.stat.executeUpdate(sql);
        conAndClos.close();
    }

    /**
     * Изменение значения тексовых ячеек в строке
     *
     * @param tableName  - название таблицы
     * @param columnName - название столбца
     * @param values     - массив значений(старое и новое)
     * @throws SQLException - ошибка с SQL кодом и базой данных
     */
    public void updateElementString(String tableName, String columnName, String[] values) throws SQLException {
        conAndClos.connect();
        sql = "UPDATE `" + tableName + "` SET `" + columnName + "` ='" + values[0] + "' WHERE `" + tableName + "`." + "`Код студента` = " + Integer.parseInt(values[1]);
        conAndClos.stat.executeUpdate(sql);
        conAndClos.close();
    }
}
