package DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ConAndClos {
    protected Connection conn;
    public Statement stat;

    /**
     * Подключение к базе данных
     */
    public void connect() {
        try {
            conn = DriverManager.getConnection("jdbc:mysql://localhost/student_data?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "mysql", "mysql");
            stat = conn.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Отключение от базы данных
     *
     * @throws SQLException - ошибка с SQL кодом и базой данных
     */
    public void close() throws SQLException {
        stat.close();
        conn.close();
    }
}
