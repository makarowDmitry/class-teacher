import DB.ConAndClos;
import DB.EditingDataBase;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import pojo.ParentsTable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Parents {
    ConAndClos conAndClos = new ConAndClos();

    private ObservableList<ParentsTable> table = FXCollections.observableArrayList();

    @FXML
    private TableView<ParentsTable> tableView;

    @FXML
    private TableColumn<ParentsTable, Integer> cod;

    @FXML
    private TableColumn<ParentsTable, String> fullFamily;

    @FXML
    private TableColumn<ParentsTable, String> FIOMum;

    @FXML
    private TableColumn<ParentsTable, String> addressMum;

    @FXML
    private TableColumn<ParentsTable, String> phoneMum;

    @FXML
    private TableColumn<ParentsTable, String> placeWorkMum;

    @FXML
    private TableColumn<ParentsTable, String> FIODad;

    @FXML
    private TableColumn<ParentsTable, String> addressDad;

    @FXML
    private TableColumn<ParentsTable, String> phoneDad;

    @FXML
    private TableColumn<ParentsTable, String> placeWorkDad;

    /**
     * Отображает таблицу "Родители"
     *
     * @throws SQLException - ошибка с SQL кодом и базой данных
     */
    public void displayTable() throws SQLException {
        tableView.getItems().clear();
        conAndClos.connect();
        ResultSet resultSet = conAndClos.stat.executeQuery("SELECT * FROM `родители`");

        ArrayList<Integer> arrCod = new ArrayList<>();
        ArrayList<String> arrFullFamily = new ArrayList<>();
        ArrayList<String> arrFIOMum = new ArrayList<>();
        ArrayList<String> arrAddressMum = new ArrayList<>();
        ArrayList<String> arrPhoneMum = new ArrayList<>();
        ArrayList<String> arrPlaceWorkMum = new ArrayList<>();
        ArrayList<String> arrFIODad = new ArrayList<>();
        ArrayList<String> arrAddressDad = new ArrayList<>();
        ArrayList<String> arrPhoneDad = new ArrayList<>();
        ArrayList<String> arrPlaceWorkDad = new ArrayList<>();
        while (resultSet.next()) {
            arrCod.add(resultSet.getInt(3));
            arrFullFamily.add(resultSet.getString(2));
            arrFIOMum.add(resultSet.getString(4));
            arrAddressMum.add(resultSet.getString(5));
            arrPhoneMum.add(resultSet.getString(6));
            arrPlaceWorkMum.add(resultSet.getString(7));
            arrFIODad.add(resultSet.getString(8));
            arrAddressDad.add(resultSet.getString(9));
            arrPhoneDad.add(resultSet.getString(10));
            arrPlaceWorkDad.add(resultSet.getString(11));

        }
        conAndClos.close();

        cod.setCellValueFactory(new PropertyValueFactory<ParentsTable, Integer>("cod"));
        fullFamily.setCellValueFactory(new PropertyValueFactory<ParentsTable, String>("fullFamily"));
        FIOMum.setCellValueFactory(new PropertyValueFactory<ParentsTable, String>("FIOMum"));
        addressMum.setCellValueFactory(new PropertyValueFactory<ParentsTable, String>("addressMum"));
        phoneMum.setCellValueFactory(new PropertyValueFactory<ParentsTable, String>("phoneMum"));
        placeWorkMum.setCellValueFactory(new PropertyValueFactory<ParentsTable, String>("placeWorkMum"));
        FIODad.setCellValueFactory(new PropertyValueFactory<ParentsTable, String>("FIODad"));
        addressDad.setCellValueFactory(new PropertyValueFactory<ParentsTable, String>("addressDad"));
        phoneDad.setCellValueFactory(new PropertyValueFactory<ParentsTable, String>("phoneDad"));
        placeWorkDad.setCellValueFactory(new PropertyValueFactory<ParentsTable, String>("placeWorkDad"));
        tableView.setItems(table);

        for (int i = 0; i < arrCod.size(); i++) {
            table.add(new ParentsTable(arrCod.get(i), arrFullFamily.get(i), arrFIOMum.get(i), arrAddressMum.get(i), arrPhoneMum.get(i), arrPlaceWorkMum.get(i), arrFIODad.get(i), arrAddressDad.get(i), arrPhoneDad.get(i), arrPlaceWorkDad.get(i)));
        }

    }

    EditingDataBase editingDataBase = new EditingDataBase();

    @FXML
    TextField delete;

    /**
     * Удаляет строку из таблицы "Родители"
     *
     * @throws SQLException - ошибка с SQL кодом и базой данных
     */
    public void deleteLineParents() throws SQLException {
        editingDataBase.deleteLine("родители", "Код студента", Integer.parseInt(delete.getText()));

    }

    Stage primaryStage = new Stage();

    /**
     * Открывает окно редактирования данных в таблице "Родители"
     *
     * @throws Exception
     */
    public void inputMenuEditing() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("tablesediting/parentsediting.fxml"));

        primaryStage.setTitle("Редактирование таблицы");
        primaryStage.setScene(new Scene(root, 600, 500));
        primaryStage.show();
    }

    /**
     * Открывает окно добавления строки в таблицу "Родители"
     *
     * @throws Exception
     */
    public void inputMenuAdd() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("adddata/parents.fxml"));

        primaryStage.setTitle("Добавление данных в таблицу");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }
}
