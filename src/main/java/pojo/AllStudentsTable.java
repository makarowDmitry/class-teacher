package pojo;

public class AllStudentsTable {
    private String FIO;
    private String date;
    private String address;
    private String phone;

    public AllStudentsTable(String FIO, String date, String address, String phone) {
        this.FIO = FIO;
        this.date = date;
        this.address = address;
        this.phone = phone;
    }

    public String getFIO() {
        return FIO;
    }

    public void setFIO(String FIO) {
        this.FIO = FIO;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
