package pojo;

public class AcademicPerformanceTable {
    private int cod;
    private String FIOstud;
    private String nameLesson;
    private int assessment;
    private String FIOteacher;

    public AcademicPerformanceTable(int cod, String FIOstud, String nameLesson, int assessment, String FIOteacher) {
        this.cod = cod;
        this.FIOstud = FIOstud;
        this.nameLesson = nameLesson;
        this.assessment = assessment;
        this.FIOteacher = FIOteacher;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getFIOstud() {
        return FIOstud;
    }

    public void setFIOstud(String FIOstud) {
        this.FIOstud = FIOstud;
    }

    public String getNameLesson() {
        return nameLesson;
    }

    public void setNameLesson(String nameLesson) {
        this.nameLesson = nameLesson;
    }

    public int getAssessment() {
        return assessment;
    }

    public void setAssessment(int assessment) {
        this.assessment = assessment;
    }

    public String getFIOteacher() {
        return FIOteacher;
    }

    public void setFIOteacher(String FIOteacher) {
        this.FIOteacher = FIOteacher;
    }
}
