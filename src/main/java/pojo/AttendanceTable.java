package pojo;

public class AttendanceTable {
    private int cod;
    private String FIO;
    private String date;
    private String first;
    private String second;
    private String third;

    public AttendanceTable(int cod, String FIO, String date, String first, String second, String third) {
        this.cod = cod;
        this.FIO = FIO;
        this.date = date;
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getFIO() {
        return FIO;
    }

    public void setFIO(String FIO) {
        this.FIO = FIO;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getfirst() {
        return first;
    }

    public void setfirst(String first) {
        this.first = first;
    }

    public String getsecond() {
        return second;
    }

    public void setsecond(String second) {
        this.second = second;
    }

    public String getthird() {
        return third;
    }

    public void setthird(String third) {
        this.third = third;
    }
}
