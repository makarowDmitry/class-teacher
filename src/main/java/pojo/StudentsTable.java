package pojo;

public class StudentsTable {
    private int cod;
    private String FIOStudent;
    private String dateOfBirth;
    private String address;
    private String phone;

    public StudentsTable(int cod, String FIOStudent, String dateOfBirth, String address, String phone) {
        this.cod = cod;
        this.FIOStudent = FIOStudent;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
        this.phone = phone;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getFIOStudent() {
        return FIOStudent;
    }

    public void setFIOStudent(String FIOStudent) {
        this.FIOStudent = FIOStudent;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
