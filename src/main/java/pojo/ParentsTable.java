package pojo;

public class ParentsTable {
    private int cod;
    private String fullFamily;
    private String FIOMum;
    private String addressMum;
    private String phoneMum;
    private String placeWorkMum;
    private String FIODad;
    private String addressDad;
    private String phoneDad;
    private String placeWorkDad;

    public ParentsTable(int cod, String fullFamily, String FIOMum, String addressMum, String phoneMum, String placeWorkMum, String FIODad, String addressDad, String phoneDad, String placeWorkDad) {
        this.cod = cod;
        this.fullFamily = fullFamily;
        this.FIOMum = FIOMum;
        this.addressMum = addressMum;
        this.phoneMum = phoneMum;
        this.placeWorkMum = placeWorkMum;
        this.FIODad = FIODad;
        this.addressDad = addressDad;
        this.phoneDad = phoneDad;
        this.placeWorkDad = placeWorkDad;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getFullFamily() {
        return fullFamily;
    }

    public void setFullFamily(String fullFamily) {
        this.fullFamily = fullFamily;
    }

    public String getFIOMum() {
        return FIOMum;
    }

    public void setFIOMum(String FIOMum) {
        this.FIOMum = FIOMum;
    }

    public String getAddressMum() {
        return addressMum;
    }

    public void setAddressMum(String addressMum) {
        this.addressMum = addressMum;
    }

    public String getPhoneMum() {
        return phoneMum;
    }

    public void setPhoneMum(String phoneMum) {
        this.phoneMum = phoneMum;
    }

    public String getPlaceWorkMum() {
        return placeWorkMum;
    }

    public void setPlaceWorkMum(String placeWorkMum) {
        this.placeWorkMum = placeWorkMum;
    }

    public String getFIODad() {
        return FIODad;
    }

    public void setFIODad(String FIODad) {
        this.FIODad = FIODad;
    }

    public String getAddressDad() {
        return addressDad;
    }

    public void setAddressDad(String addressDad) {
        this.addressDad = addressDad;
    }

    public String getPhoneDad() {
        return phoneDad;
    }

    public void setPhoneDad(String phoneDad) {
        this.phoneDad = phoneDad;
    }

    public String getPlaceWorkDad() {
        return placeWorkDad;
    }

    public void setPlaceWorkDad(String placeWorkDad) {
        this.placeWorkDad = placeWorkDad;
    }
}
