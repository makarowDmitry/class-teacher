package pojo;

public class PersonalDataTable {
    private int cod;
    private long passportData;
    private long INN;
    private long SNILS;

    public PersonalDataTable(int cod, long passportData, long INN, long SNILS) {
        this.cod = cod;
        this.passportData = passportData;
        this.INN = INN;
        this.SNILS = SNILS;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public long getPassportData() {
        return passportData;
    }

    public void setPassportData(long passportData) {
        this.passportData = passportData;
    }

    public long getINN() {
        return INN;
    }

    public void setINN(long INN) {
        this.INN = INN;
    }

    public long getSNILS() {
        return SNILS;
    }

    public void setSNILS(long SNILS) {
        this.SNILS = SNILS;
    }
}
