package addData;

import Check.CheckInputData;
import DB.ConAndClos;
import DB.GetData;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.sql.SQLException;

public class AddDataAcademicPerfomance {
    ConAndClos conAndClos = new ConAndClos();
    CheckInputData checkInputData = new CheckInputData();

    @FXML
    TextField cod;

    @FXML
    TextField nameLesson;

    @FXML
    TextField assessment;

    @FXML
    TextField FIOteacher;


    @FXML
    Label error;

    GetData getData = new GetData();
    /**
     * Добавляет строку в базу данных "Успеваемость"
     *
     * @throws SQLException - ошибка с SQL кодом и базой данных
     */
    public void addLine() throws SQLException {
        conAndClos.connect();
        if (checkInputData.checkText(FIOteacher.getText()) && checkInputData.checkAssessment(assessment.getText())) {
            String sql = "INSERT `успеваемость` (`Код студента`,`ФИО студента`,`Название предмета`,`Оценка за семестр`, `ФИО преподавателя`) VALUES(" + Integer.parseInt(cod.getText()) + ",'" + getData.getFIO(cod.getText()) + "','" + nameLesson.getText() + "'," + Integer.parseInt(assessment.getText()) + ",'" + FIOteacher.getText() + "')";
            try{
                conAndClos.stat.executeUpdate(sql);
            }catch (SQLException e){
                error.setText("Данные введены некорректно или отсутствует соединение с базой данных");
            }

            error.setText("");
        } else {
            error.setText("Данные введены некорректно");
        }
        conAndClos.close();
    }
}
