package addData;

import Check.CheckInputData;
import DB.ConAndClos;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.sql.SQLException;

public class AddDataPersonalData {
    ConAndClos conAndClos = new ConAndClos();
    CheckInputData checkInputData = new CheckInputData();
    @FXML
    TextField cod;

    @FXML
    TextField passData;

    @FXML
    TextField INN;

    @FXML
    TextField SNILS;

    @FXML
    Label error;
    /**
     * Добавляет строку в базу данных "Личные данные"
     *
     * @throws SQLException - ошибка с SQL кодом и базой данных
     */
    public void addLine() throws SQLException {
        conAndClos.connect();
        if (checkInputData.checkNumber(passData.getText(), 10) && checkInputData.checkNumber(INN.getText(), 12) && checkInputData.checkNumber(SNILS.getText(), 11)) {
            String sql = "INSERT `личные данные` (`Код студента`,`Паспортные данные`,`ИНН`,`СНИЛС`) VALUES(" + Integer.parseInt(cod.getText()) + "," + Long.parseLong(passData.getText()) + "," + Long.parseLong(INN.getText()) + "," + Long.parseLong(SNILS.getText()) + ")";
            try{
                conAndClos.stat.executeUpdate(sql);
            }catch (SQLException e){
                error.setText("Данные введены некорректно или отсутствует соединение с базой данных");
            }
            error.setText("");
        }
        else {
            error.setText("Данные введены некорректно");
        }
        conAndClos.close();

    }
}
