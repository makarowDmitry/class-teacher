package addData;

import Check.CheckInputData;
import DB.ConAndClos;
import DB.GetData;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;

import java.sql.SQLException;

public class AddDataAttendance {
    ConAndClos conAndClos = new ConAndClos();
    CheckInputData checkInputData = new CheckInputData();
    @FXML
    TextField cod;

    @FXML
    TextField date;

    @FXML
    RadioButton oneLessonTrue;

    @FXML
    RadioButton oneLessonFalse;

    @FXML
    RadioButton twoLessonTrue;

    @FXML
    RadioButton twoLessonFalse;

    @FXML
    RadioButton threeLessonTrue;

    @FXML
    RadioButton threeLessonFalse;

    @FXML
    Label error;

    GetData getData = new GetData();
    /**
     * Добавляет строку в базу данных "Посещаемость"
     *
     * @throws SQLException - ошибка с SQL кодом и базой данных
     */
    public void addLine() throws SQLException {
        conAndClos.connect();
        ToggleGroup groupOne = new ToggleGroup();
        oneLessonTrue.setToggleGroup(groupOne);
        oneLessonFalse.setToggleGroup(groupOne);
        ToggleGroup groupTwo = new ToggleGroup();
        twoLessonTrue.setToggleGroup(groupTwo);
        twoLessonFalse.setToggleGroup(groupTwo);
        ToggleGroup groupThree = new ToggleGroup();
        threeLessonTrue.setToggleGroup(groupThree);
        threeLessonFalse.setToggleGroup(groupThree);

        RadioButton selectionOne = (RadioButton) groupOne.getSelectedToggle();
        RadioButton selectionTwo = (RadioButton) groupTwo.getSelectedToggle();
        RadioButton selectionThree = (RadioButton) groupThree.getSelectedToggle();

        if (checkInputData.checkDate(date.getText())) {
            String sql = "INSERT `посещаемость` (`Код студента`,`ФИО студента`,`Дата`,`Первая пара`, `Вторая пара`, `Третья пара`) VALUES(" + Integer.parseInt(cod.getText()) + ",'" + getData.getFIO(cod.getText()) + "','" + date.getText() + "','" + selectionOne.getText() + "','" + selectionTwo.getText() + "','" + selectionThree.getText() + "')";
            try{
                conAndClos.stat.executeUpdate(sql);
            }catch (SQLException e){
                error.setText("Данные введены некорректно или отсутствует соединение с базой данных");
            }
            error.setText("");
        }else {
            error.setText("Данные введены некорректно");
        }

        conAndClos.close();
    }
}
