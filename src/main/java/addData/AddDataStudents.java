package addData;

import Check.CheckInputData;
import DB.ConAndClos;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.sql.SQLException;

public class AddDataStudents {
    ConAndClos conAndClos = new ConAndClos();
    CheckInputData checkInputData = new CheckInputData();

    @FXML
    TextField cod;

    @FXML
    TextField FIO;

    @FXML
    TextField date;

    @FXML
    TextField address;

    @FXML
    TextField phone;

    @FXML
    Label error;
    /**
     * Добавляет строку в базу данных "Студенты"
     *
     * @throws SQLException - ошибка с SQL кодом и базой данных
     */
    public void addLine() throws SQLException {
        conAndClos.connect();
        if (checkInputData.checkText(FIO.getText()) && checkInputData.checkDate(date.getText()) && checkInputData.checkPhone(phone.getText())) {
            String sql = "INSERT `студенты` (`Код студента`,`ФИО студента`,`Дата рождения`,`Адрес`, `Телефон`) VALUES(" + Integer.parseInt(cod.getText()) + ",'" + FIO.getText() + "','" + date.getText() + "','" + address.getText() + "','" + phone.getText() + "')";
            try{
                conAndClos.stat.executeUpdate(sql);
            }catch (SQLException e){
                error.setText("Данные введены некорректно или отсутствует соединение с базой данных");
            }
            error.setText("");
        }else {
            error.setText("Данные введены некорректно");
        }
        conAndClos.close();
    }
}
