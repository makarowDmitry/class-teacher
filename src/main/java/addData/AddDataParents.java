package addData;

import Check.CheckInputData;
import DB.ConAndClos;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;

import java.sql.SQLException;

public class AddDataParents {
    ConAndClos conAndClos = new ConAndClos();
    CheckInputData checkInputData = new CheckInputData();
    @FXML
    TextField cod;

    @FXML
    RadioButton mumAndDad;
    @FXML
    RadioButton mum;
    @FXML
    RadioButton dad;

    @FXML
    TextField FIOMum;

    @FXML
    TextField addressMum;

    @FXML
    TextField phoneMum;

    @FXML
    TextField workMum;

    @FXML
    TextField workDad;

    @FXML
    TextField phoneDad;

    @FXML
    TextField addressDad;

    @FXML
    TextField FIODad;

    @FXML
    Label error;

    /**
     * Добавляет строку в базу данных "Родители"
     *
     * @throws SQLException - ошибка с SQL кодом и базой данных
     */
    public void addLine() throws SQLException {
        conAndClos.connect();
        String sql = "";
        ToggleGroup oneGroup = new ToggleGroup();
        mumAndDad.setToggleGroup(oneGroup);
        mum.setToggleGroup(oneGroup);
        dad.setToggleGroup(oneGroup);

        RadioButton selection = (RadioButton) oneGroup.getSelectedToggle();

            switch (selection.getText()) {
                case "Полная":
                    if (checkInputData.checkText(FIODad.getText()) && checkInputData.checkText(FIOMum.getText()) && checkInputData.checkPhone(phoneMum.getText()) && checkInputData.checkPhone(phoneDad.getText())) {
                        error.setText("");
                        sql = "INSERT `родители` (`Полная семья`,`Код студента`,`ФИО мамы`,`Адрес мамы`,`Телефон мамы`,`Место работы мамы`,`ФИО папы`,`Адрес папы`,`Телефон папы`,`Место работы папы`) VALUES('" + selection.getText() + "'," + Integer.parseInt(cod.getText()) + ",'" + FIOMum.getText() + "','" + addressMum.getText() + "'," + Long.parseLong(phoneMum.getText()) + ",'" + workMum.getText() + "','" + FIODad.getText() + "','" + addressDad.getText() + "'," + Long.parseLong(phoneDad.getText()) + ",'" + workDad.getText() + "')";

                    }else {
                        error.setText("Данные введены некорректно");
                    }
                    break;
                case "Воспитывает Мама":
                    if(checkInputData.checkText(FIOMum.getText()) && checkInputData.checkPhone(phoneMum.getText())) {
                        sql = "INSERT `родители` (`Полная семья`,`Код студента`,`ФИО мамы`,`Адрес мамы`,`Телефон мамы`,`Место работы мамы`,`ФИО папы`,`Адрес папы`,`Телефон папы`,`Место работы папы`) VALUES('" + selection.getText() + "'," + Integer.parseInt(cod.getText()) + ",'" + FIOMum.getText() + "','" + addressMum.getText() + "'," + Long.parseLong(phoneMum.getText()) + ",'" + workMum.getText() + "'," + null + "," + null + "," + null + "," + null + ")";
                        error.setText("");
                    }else {
                        error.setText("Данные введены некорректно");
                    }
                    break;
                case "Воспитывает Папа":
                    if(checkInputData.checkText(FIODad.getText()) && checkInputData.checkPhone(phoneDad.getText())) {
                        sql = "INSERT `родители` (`Полная семья`,`Код студента`,`ФИО мамы`,`Адрес мамы`,`Телефон мамы`,`Место работы мамы`,`ФИО папы`,`Адрес папы`,`Телефон папы`,`Место работы папы`) VALUES('" + selection.getText() + "'," + Integer.parseInt(cod.getText()) + "," + null + "," + null + "," + null + "," + null + ",'" + FIODad.getText() + "','" + addressDad.getText() + "','" + phoneDad.getText() + "','" + workDad.getText() + "')";
                        error.setText("");
                    }else {
                        error.setText("Данные введены некорректно");
                    }
                    break;
            }

        try{
            conAndClos.stat.executeUpdate(sql);
        }catch (SQLException e){
            error.setText("Данные введены некорректно или отсутствует соединение с базой данных");
        }
        conAndClos.close();

    }
}
