import DB.ConAndClos;
import DB.EditingDataBase;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import pojo.StudentsTable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Students {

    ConAndClos conAndClos = new ConAndClos();

    private ObservableList<StudentsTable> table = FXCollections.observableArrayList();

    @FXML
    private TableView<StudentsTable> tableView;

    @FXML
    private TableColumn<StudentsTable, Integer> cod;

    @FXML
    private TableColumn<StudentsTable, String> FIO;

    @FXML
    private TableColumn<StudentsTable, String> date;

    @FXML
    private TableColumn<StudentsTable, String> address;

    @FXML
    private TableColumn<StudentsTable, String> phone;

    /**
     * Отображает таблицу "Студенты"
     *
     * @throws SQLException - ошибка с SQL кодом и базой данных
     */
    public void displayTable() throws SQLException {
        tableView.getItems().clear();
        conAndClos.connect();
        ResultSet resultSet = conAndClos.stat.executeQuery("SELECT * FROM `студенты`");

        ArrayList<Integer> arrCod = new ArrayList<>();
        ArrayList<String> arrFIO = new ArrayList<>();
        ArrayList<String> arrDate = new ArrayList<>();
        ArrayList<String> arrAddress = new ArrayList<>();
        ArrayList<String> arrPhone = new ArrayList<>();
        while (resultSet.next()) {
            arrCod.add(resultSet.getInt(2));
            arrFIO.add(resultSet.getString(3));
            arrDate.add(resultSet.getString(4));
            arrAddress.add(resultSet.getString(5));
            arrPhone.add(resultSet.getString(6));
        }
        conAndClos.close();

        cod.setCellValueFactory(new PropertyValueFactory<StudentsTable, Integer>("cod"));
        FIO.setCellValueFactory(new PropertyValueFactory<StudentsTable, String>("FIOStudent"));
        date.setCellValueFactory(new PropertyValueFactory<StudentsTable, String>("dateOfBirth"));
        address.setCellValueFactory(new PropertyValueFactory<StudentsTable, String>("address"));
        phone.setCellValueFactory(new PropertyValueFactory<StudentsTable, String>("phone"));
        tableView.setItems(table);

        for (int i = 0; i < arrCod.size(); i++) {
            table.add(new StudentsTable(arrCod.get(i), arrFIO.get(i), arrDate.get(i), arrAddress.get(i), arrPhone.get(i)));
        }

    }

    EditingDataBase editingDataBase = new EditingDataBase();

    @FXML
    TextField delete;

    /**
     * Удаляет строку из таблицы "Студенты"
     *
     * @throws SQLException - ошибка с SQL кодом и базой данных
     */
    public void deleteLineStudents() throws SQLException {
        editingDataBase.deleteLine("студенты", "Код студента", Integer.parseInt(delete.getText()));

    }

    Stage primaryStage = new Stage();

    /**
     * Открывает окно редактирования данных в таблице "Студенты"
     *
     * @throws Exception
     */
    public void inputMenuEditing() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("tablesediting/studentsediting.fxml"));

        primaryStage.setTitle("Редактирование таблицы");
        primaryStage.setScene(new Scene(root, 600, 270));
        primaryStage.show();
    }

    /**
     * Открывает окно добавления строки в таблицу "Студенты"
     *
     * @throws Exception
     */
    public void inputMenuAdd() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("adddata/students.fxml"));

        primaryStage.setTitle("Добавление данных в таблицу");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }
}
