import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ControllerMenuTwo {
    Stage primaryStage = new Stage();

    /**
     * Открывает окно для редактирования и отображения данных в таблице "Родители"
     *
     * @throws Exception
     */
    public void parentsTable() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("tables/parents.fxml"));

        primaryStage.setTitle("Таблица родители");
        primaryStage.setScene(new Scene(root, 1000, 400));
        primaryStage.show();
    }

    /**
     * Открывает окно для редактирования и отображения данных в таблице "Личные данные"
     *
     * @throws Exception
     */
    public void personalDataTable() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("tables/personaldata.fxml"));

        primaryStage.setTitle("Таблица личные данные");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }

    /**
     * Открывает окно для редактирования и отображения данных в таблице "Студенты"
     *
     * @throws Exception
     */
    public void studentsTable() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("tables/students.fxml"));

        primaryStage.setTitle("Таблица студенты");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }
}
