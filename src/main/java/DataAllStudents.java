import DB.ConAndClos;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import pojo.AllStudentsTable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DataAllStudents {
    ConAndClos conAndClos = new ConAndClos();

    private ObservableList<AllStudentsTable> table = FXCollections.observableArrayList();

    @FXML
    private TableView<AllStudentsTable> tableView;

    @FXML
    private TableColumn<AllStudentsTable, String> FIO;

    @FXML
    private TableColumn<AllStudentsTable, String> date;

    @FXML
    private TableColumn<AllStudentsTable, String> address;

    @FXML
    private TableColumn<AllStudentsTable, String> phone;

    /**
     * Отображение информации о всех студентах
     *
     * @throws SQLException - ошибка с SQL кодом и базой данных
     */
    public void displayTable() throws SQLException {
        tableView.getItems().clear();
        conAndClos.connect();
        ResultSet resultSet = conAndClos.stat.executeQuery("SELECT * FROM `студенты`");

        ArrayList<String> arrFIO = new ArrayList<>();
        ArrayList<String> arrDate = new ArrayList<>();
        ArrayList<String> arrAddress = new ArrayList<>();
        ArrayList<String> arrPhone = new ArrayList<>();
        while (resultSet.next()) {
            arrFIO.add(resultSet.getString(3));
            arrDate.add(resultSet.getString(4));
            arrAddress.add(resultSet.getString(5));
            arrPhone.add(resultSet.getString(6));
        }
        conAndClos.close();

        FIO.setCellValueFactory(new PropertyValueFactory<AllStudentsTable, String>("FIO"));
        date.setCellValueFactory(new PropertyValueFactory<AllStudentsTable, String>("date"));
        address.setCellValueFactory(new PropertyValueFactory<AllStudentsTable, String>("address"));
        phone.setCellValueFactory(new PropertyValueFactory<AllStudentsTable, String>("phone"));
        tableView.setItems(table);

        for (int i = 0; i < arrFIO.size(); i++) {
            table.add(new AllStudentsTable(arrFIO.get(i), arrDate.get(i), arrAddress.get(i), arrPhone.get(i)));
        }

    }

}
