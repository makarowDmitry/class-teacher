package Check;

public class CheckInputData {

    public boolean checkText(String text){
        return text.matches("\\D+");
    }

    public boolean checkDate(String date){
        return date.matches("([0-9]{2}).([0-9]{2}).([0-9]{4})");

    }

    public boolean checkPhone(String phone){
        return phone.matches("^89\\d{9}$");
    }

    public boolean checkAssessment(String assessment){
        return assessment.matches("[1-5]?");
    }

    public boolean checkNumber(String data, int num){
        return data.matches("\\d{"+num+"}$");
    }
}
