import DB.ConAndClos;
import DB.EditingDataBase;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import pojo.PersonalDataTable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PersonalData {
    ConAndClos conAndClos = new ConAndClos();

    private ObservableList<PersonalDataTable> table = FXCollections.observableArrayList();

    @FXML
    private TableView<PersonalDataTable> tableView;

    @FXML
    private TableColumn<PersonalDataTable, Integer> cod;

    @FXML
    private TableColumn<PersonalDataTable, Long> passData;

    @FXML
    private TableColumn<PersonalDataTable, Long> INN;

    @FXML
    private TableColumn<PersonalDataTable, Long> SNILS;

    /**
     * Отображает таблицу "Личные данные"
     *
     * @throws SQLException - ошибка с SQL кодом и базой данных
     */
    public void displayTable() throws SQLException {
        tableView.getItems().clear();
        conAndClos.connect();
        ResultSet resultSet = conAndClos.stat.executeQuery("SELECT * FROM `личные данные`");

        ArrayList<Integer> arrCod = new ArrayList<>();
        ArrayList<Long> arrPassData = new ArrayList<>();
        ArrayList<Long> arrINN = new ArrayList<>();
        ArrayList<Long> arrSNILS = new ArrayList<>();
        while (resultSet.next()) {
            arrCod.add(resultSet.getInt(2));
            arrPassData.add(resultSet.getLong(3));
            arrINN.add(resultSet.getLong(4));
            arrSNILS.add(resultSet.getLong(5));
        }
        conAndClos.close();

        cod.setCellValueFactory(new PropertyValueFactory<PersonalDataTable, Integer>("cod"));
        passData.setCellValueFactory(new PropertyValueFactory<PersonalDataTable, Long>("passportData"));
        INN.setCellValueFactory(new PropertyValueFactory<PersonalDataTable, Long>("INN"));
        SNILS.setCellValueFactory(new PropertyValueFactory<PersonalDataTable, Long>("SNILS"));
        tableView.setItems(table);

        for (int i = 0; i < arrCod.size(); i++) {
            table.add(new PersonalDataTable(arrCod.get(i), arrPassData.get(i), arrINN.get(i), arrSNILS.get(i)));
        }

    }

    EditingDataBase editingDataBase = new EditingDataBase();

    @FXML
    TextField delete;

    /**
     * Удаляет строку из таблицы "Личные данные"
     *
     * @throws SQLException - ошибка с SQL кодом и базой данных
     */
    public void deleteLinePersonalData() throws SQLException {
        editingDataBase.deleteLine("личные данные", "Код студента", Integer.parseInt(delete.getText()));

    }

    Stage primaryStage = new Stage();

    /**
     * Открывает окно редактирования данных в таблице "Личные данные"
     *
     * @throws Exception
     */
    public void inputMenuEditing() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("tablesediting/personaldataediting.fxml"));

        primaryStage.setTitle("Редактирование таблицы");
        primaryStage.setScene(new Scene(root, 600, 220));
        primaryStage.show();
    }

    /**
     * Открывает окно добавления строки в таблицу "Личные данные"
     *
     * @throws Exception
     */
    public void inputMenuAdd() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("adddata/personalData.fxml"));

        primaryStage.setTitle("Добавление данных в таблицу");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }


}
