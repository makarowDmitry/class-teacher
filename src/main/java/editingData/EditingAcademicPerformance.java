package editingData;

import Check.CheckInputData;
import DB.EditingDataBase;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.sql.SQLException;

public class EditingAcademicPerformance {
    EditingDataBase editingDataBase = new EditingDataBase();
    CheckInputData checkInputData = new CheckInputData();

    @FXML
    TextField oldCod;
    @FXML
    TextField newCod;
    @FXML
    TextField newFIO;
    @FXML
    TextField newNameLesson;
    @FXML
    TextField newAssessment;
    @FXML
    TextField newFIOTeacher;

    @FXML
    TextField cod;


    @FXML
    Label errorFIO;

    @FXML
    Label errorFIOTeacher;

    @FXML
    Label errorAssessment;
    /**
     * Изменение данных в ячейках для таблицы "Успеваемость"
     *
     * @throws SQLException - ошибка с SQL кодом и базой данных
     */
    public void updateData() throws SQLException {
        if (!(newCod.getText().equals(""))) {
            int[] arrValue = {Integer.parseInt(newCod.getText()), Integer.parseInt(oldCod.getText())};
            editingDataBase.updateElementInt("успеваемость", "Код студента", arrValue);
        }

        if (!(newFIO.getText().equals("")) && checkInputData.checkText(newFIO.getText()) ) {
            String[] arrValue = {newFIO.getText(), cod.getText()};
            editingDataBase.updateElementString("успеваемость", "ФИО студента", arrValue);
            errorFIO.setText("");
        }else{
            errorFIO.setText("Данные введены некорректно");
        }
        if (!(newNameLesson.getText().equals(""))) {
            String[] arrValue = {newNameLesson.getText(), cod.getText()};
            editingDataBase.updateElementString("успеваемость", "Название предмета", arrValue);
        }
        if (!(newAssessment.getText().equals("")) && checkInputData.checkAssessment(newAssessment.getText())) {
            int[] arrValue = {Integer.parseInt(newAssessment.getText()), Integer.parseInt(cod.getText())};
            editingDataBase.updateElementInt("успеваемость", "Оценка за семестр", arrValue);
            errorAssessment.setText("");
        }else {
            errorAssessment.setText("Данные введены некорректно");
        }
        if (!(newFIOTeacher.getText().equals("")) && checkInputData.checkText(errorFIOTeacher.getText())) {
            String[] arrValue = {newFIOTeacher.getText(), cod.getText()};
            editingDataBase.updateElementString("успеваемость", "ФИО преподавателя", arrValue);
            errorFIOTeacher.setText("");
        }else {
            errorFIOTeacher.setText("Данные введены некорректно");
        }
    }
}
