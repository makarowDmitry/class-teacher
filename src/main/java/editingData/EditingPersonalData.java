package editingData;

import Check.CheckInputData;
import DB.EditingDataBase;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.sql.SQLException;


public class EditingPersonalData {

    EditingDataBase editingDataBase = new EditingDataBase();
    CheckInputData checkInputData = new CheckInputData();
    @FXML
    TextField oldCod;
    @FXML
    TextField newCod;
    @FXML
    TextField newPassData;
    @FXML
    TextField newINN;
    @FXML
    TextField newSNILS;

    @FXML
    TextField cod;

    @FXML
    Label errorPassData;
    @FXML
    Label errorINN;
    @FXML
    Label errorSNILS;

    /**
     * Изменение данных в ячейках для таблицы "Личные данные"
     *
     * @throws SQLException - ошибка с SQL кодом и базой данных
     */
    public void updateData() throws SQLException {
        if (!(newCod.getText().equals(""))) {
            int[] arrValue = {Integer.parseInt(newCod.getText()), Integer.parseInt(oldCod.getText())};
            editingDataBase.updateElementInt("личные данные", "Код студента", arrValue);
        }
        if (!(newPassData.getText().equals("")) && checkInputData.checkNumber(newPassData.getText(), 10)) {
            long[] arrValue = {Long.parseLong(newPassData.getText()), Long.parseLong(cod.getText())};
            editingDataBase.updateElementLong("личные данные", "Паспортные данные", arrValue);
            errorPassData.setText("");
        }else {
            errorPassData.setText("Данные введены некорректно");
        }
        if (!(newINN.getText().equals("")) && checkInputData.checkNumber(newINN.getText(), 12)) {
            long[] arrValue = {Long.parseLong(newINN.getText()), Long.parseLong(cod.getText())};
            editingDataBase.updateElementLong("личные данные", "ИНН", arrValue);
            errorINN.setText("");
        }else {
            errorINN.setText("Данные введены некорректно");
        }
        if (!(newSNILS.getText().equals("")) && checkInputData.checkNumber(newSNILS.getText(), 11)) {
            long[] arrValue = {Long.parseLong(newSNILS.getText()), Long.parseLong(cod.getText())};
            editingDataBase.updateElementLong("личные данные", "СНИЛС", arrValue);
            errorSNILS.setText("");
        }else {
            errorSNILS.setText("Данные введены некорректно");
        }
    }
}
