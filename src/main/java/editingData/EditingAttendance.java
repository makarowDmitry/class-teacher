package editingData;

import Check.CheckInputData;
import DB.EditingDataBase;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;


import java.sql.SQLException;

public class EditingAttendance {
    EditingDataBase editingDataBase = new EditingDataBase();
    CheckInputData checkInputData = new CheckInputData();
    @FXML
    TextField oldCod;
    @FXML
    TextField newCod;
    @FXML
    TextField newFIO;
    @FXML
    TextField newDate;

    @FXML
    RadioButton oneLessonTrue;

    @FXML
    RadioButton oneLessonFalse;

    @FXML
    RadioButton twoLessonTrue;

    @FXML
    RadioButton twoLessonFalse;

    @FXML
    RadioButton threeLessonTrue;

    @FXML
    RadioButton threeLessonFalse;

    @FXML
    TextField cod;

    @FXML
    Label errorFIO;

    @FXML
    Label errorDate;

    /**
     * Изменение данных в ячейках для таблицы "Посещаемость"
     *
     * @throws SQLException - ошибка с SQL кодом и базой данных
     */
    public void updateData() throws SQLException {
        if (!(newCod.getText().equals(""))) {
            int[] arrValue = {Integer.parseInt(newCod.getText()), Integer.parseInt(oldCod.getText())};
            editingDataBase.updateElementInt("посещаемость", "Код студента", arrValue);
        }
        if (!(newFIO.getText().equals("")) && checkInputData.checkText(newFIO.getText())) {
            String[] arrValue = {newFIO.getText(), cod.getText()};
            editingDataBase.updateElementString("посещаемость", "ФИО студента", arrValue);
            errorFIO.setText("");
        }else {
            errorFIO.setText("Данные введены некорректно");
        }
        if (!(newDate.getText().equals("")) && checkInputData.checkDate(newDate.getText())) {
            String[] arrValue = {newDate.getText(), cod.getText()};
            editingDataBase.updateElementString("посещаемость", "Дата", arrValue);
            errorDate.setText("");
        }else {
            errorDate.setText("Данные введены некорректно");
        }
        ToggleGroup groupOne = new ToggleGroup();
        oneLessonTrue.setToggleGroup(groupOne);
        oneLessonFalse.setToggleGroup(groupOne);
        ToggleGroup groupTwo = new ToggleGroup();
        twoLessonTrue.setToggleGroup(groupTwo);
        twoLessonFalse.setToggleGroup(groupTwo);
        ToggleGroup groupThree = new ToggleGroup();
        threeLessonTrue.setToggleGroup(groupThree);
        threeLessonFalse.setToggleGroup(groupThree);

        RadioButton selectionOne = (RadioButton) groupOne.getSelectedToggle();
        RadioButton selectionTwo = (RadioButton) groupTwo.getSelectedToggle();
        RadioButton selectionThree = (RadioButton) groupThree.getSelectedToggle();

        if (!(selectionOne.getText().equals(""))) {
            String[] arrValue = {selectionOne.getText(), cod.getText()};
            editingDataBase.updateElementString("посещаемость", "Первая пара", arrValue);
        }

        if (!(selectionTwo.getText().equals(""))) {
            String[] arrValue = {selectionTwo.getText(), cod.getText()};
            editingDataBase.updateElementString("посещаемость", "Вторая пара", arrValue);
        }

        if (!(selectionThree.getText().equals(""))) {
            String[] arrValue = {selectionThree.getText(), cod.getText()};
            editingDataBase.updateElementString("посещаемость", "Третья пара", arrValue);
        }

    }
}
