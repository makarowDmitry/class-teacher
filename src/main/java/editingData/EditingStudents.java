package editingData;

import Check.CheckInputData;
import DB.EditingDataBase;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.sql.SQLException;

public class EditingStudents {
    EditingDataBase editingDataBase = new EditingDataBase();
    CheckInputData checkInputData = new CheckInputData();

    @FXML
    TextField oldCod;
    @FXML
    TextField newCod;
    @FXML
    TextField newFIO;
    @FXML
    TextField newDate;
    @FXML
    TextField newAddress;
    @FXML
    TextField newPhone;

    @FXML
    TextField cod;

    @FXML
    Label errorFIO;

    @FXML
    Label errorDate;

    @FXML
    Label errorPhone;
    /**
     * Изменение данных в ячейках для таблицы "Студенты"
     *
     * @throws SQLException - ошибка с SQL кодом и базой данных
     */
    public void updateData() throws SQLException {
        if (!(newCod.getText().equals(""))) {
            int[] arrValue = {Integer.parseInt(newCod.getText()), Integer.parseInt(oldCod.getText())};
            editingDataBase.updateElementInt("студенты", "Код студента", arrValue);
        }
        if (!(newFIO.getText().equals("")) && checkInputData.checkText(newFIO.getText())) {
            String[] arrValue = {newFIO.getText(), cod.getText()};
            editingDataBase.updateElementString("студенты", "ФИО студента", arrValue);
            errorFIO.setText("");
        }else {
            errorFIO.setText("Данные введены некорректно");
        }
        if (!(newDate.getText().equals("")) && checkInputData.checkDate(newDate.getText())) {
            String[] arrValue = {newDate.getText(), cod.getText()};
            editingDataBase.updateElementString("студенты", "Дата рождения", arrValue);
            errorDate.setText("");
        }else {
            errorDate.setText("Данные введены некорректно");
        }
        if (!(newAddress.getText().equals(""))) {
            String[] arrValue = {newAddress.getText(), cod.getText()};
            editingDataBase.updateElementString("студенты", "Адрес", arrValue);
        }
        if (!(newPhone.getText().equals("")) && checkInputData.checkPhone(newPhone.getText())) {
            String [] arrValue = {newPhone.getText(), cod.getText()};
            editingDataBase.updateElementString("студенты", "Телефон", arrValue);
            errorPhone.setText("");
        }else {
            errorPhone.setText("Данные введены некорректно");
        }
    }
}
