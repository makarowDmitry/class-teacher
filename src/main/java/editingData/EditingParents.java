package editingData;

import Check.CheckInputData;
import DB.EditingDataBase;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;

import java.sql.SQLException;

public class EditingParents {
    EditingDataBase editingDataBase = new EditingDataBase();
    CheckInputData checkInputData = new CheckInputData();
    @FXML
    TextField oldCod;
    @FXML
    TextField newCod;
    @FXML
    RadioButton mumAndDad;
    @FXML
    RadioButton mum;
    @FXML
    RadioButton dad;
    @FXML
    TextField newFIOMum;
    @FXML
    TextField newAddressMum;
    @FXML
    TextField newPhoneMum;
    @FXML
    TextField newWorkMum;
    @FXML
    TextField newFIODad;
    @FXML
    TextField newAddressDad;
    @FXML
    TextField newPhoneDad;
    @FXML
    TextField newWorkDad;
    @FXML
    TextField cod;

    @FXML
    Label errorFIOMum;
    @FXML
    Label errorFIODad;
    @FXML
    Label errorPhoneMum;
    @FXML
    Label errorPhoneDad;

    /**
     * Изменение данных в ячейках для таблицы "Родители"
     *
     * @throws SQLException - ошибка с SQL кодом и базой данных
     */
    public void updateData() throws SQLException {
        if (!(newCod.getText().equals(""))) {
            int[] arrValue = {Integer.parseInt(newCod.getText()), Integer.parseInt(oldCod.getText())};
            editingDataBase.updateElementInt("родители", "Код студента", arrValue);
        }

        ToggleGroup group = new ToggleGroup();
        mumAndDad.setToggleGroup(group);
        mum.setToggleGroup(group);
        dad.setToggleGroup(group);

        RadioButton selection = (RadioButton) group.getSelectedToggle();
        if (!(selection.getText().equals(""))) {
            String[] arrValue = {selection.getText(), cod.getText()};
            editingDataBase.updateElementString("родители", "Полная семья", arrValue);
        }
        if (!(newFIOMum.getText().equals("")) && checkInputData.checkText(newFIOMum.getText())) {
            String[] arrValue = {newFIOMum.getText(), cod.getText()};
            editingDataBase.updateElementString("родители", "ФИО мамы", arrValue);
            errorFIOMum.setText("");
        } else {
            errorFIOMum.setText("Данные введены некорректно");
        }
        if (!(newAddressMum.getText().equals(""))) {
            String[] arrValue = {newAddressMum.getText(), cod.getText()};
            editingDataBase.updateElementString("родители", "Адрес мамы", arrValue);
        }
        if (!(newPhoneMum.getText().equals("")) && checkInputData.checkPhone(newPhoneMum.getText())) {
            String[] arrValue = {newPhoneMum.getText(), cod.getText()};
            editingDataBase.updateElementString("родители", "Телефон мамы", arrValue);
            errorPhoneMum.setText("");
        }else {
            errorPhoneMum.setText("Данные введены некорректно");
        }
        if (!(newWorkMum.getText().equals(""))) {
            String[] arrValue = {newWorkMum.getText(), cod.getText()};
            editingDataBase.updateElementString("родители", "Место работы мамы", arrValue);
        }
        if (!(newFIODad.getText().equals("")) && checkInputData.checkText(newFIODad.getText())) {
            String[] arrValue = {newFIODad.getText(), cod.getText()};
            editingDataBase.updateElementString("родители", "ФИО папы", arrValue);
            errorFIODad.setText("");
        } else {
            errorFIODad.setText("Данные введены некорректно");
        }
        if (!(newAddressDad.getText().equals(""))) {
            String[] arrValue = {newAddressDad.getText(), cod.getText()};
            editingDataBase.updateElementString("родители", "Адрес папы", arrValue);
        }
        if (!(newPhoneDad.getText().equals("")) && checkInputData.checkPhone(newPhoneDad.getText())) {
            String[] arrValue = {newPhoneDad.getText(), cod.getText()};
            editingDataBase.updateElementString("родители", "Телефон папы", arrValue);
            errorPhoneDad.setText("");
        }else {
            errorPhoneDad.setText("Данные введены некорректно");
        }
        if (!(newWorkDad.getText().equals(""))) {
            String[] arrValue = {newWorkDad.getText(), cod.getText()};
            editingDataBase.updateElementString("родители", "Место работы папы", arrValue);
        }
    }

}
