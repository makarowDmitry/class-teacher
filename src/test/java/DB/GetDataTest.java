package DB;

import org.junit.Test;

import java.sql.SQLException;

import static org.junit.Assert.*;

public class GetDataTest {

    GetData getData = new GetData();

    @Test
    public void checkGetDataTrue() throws SQLException {
        assertEquals("Орлов М. М", getData.getFIO("1"));
    }

    @Test
    public void checkGetDataFalse() throws SQLException {
        boolean check;
        check = !"Пушкин А.С".equals(getData.getFIO("1"));
        assertTrue(check);
    }

}