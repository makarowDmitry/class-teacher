package DB;

import org.junit.Test;

import java.sql.SQLException;

public class EditingDataBaseTest {

    EditingDataBase editingDataBase = new EditingDataBase();

    ConAndClos conAndClos = new ConAndClos();

    @Test
    public void deleteLineTest() throws SQLException {
        conAndClos.connect();
        String tableName = "успеваемость";
        String columnName = "Код студента";
        int value = 2;
        editingDataBase.deleteLine(tableName, columnName, value);
        conAndClos.close();
    }


    @Test
    public void updateElementStringTest() throws  SQLException{
        conAndClos.connect();
        String tableName = "родители";
        String columnName = "ФИО мамы";
        String [] values = {"Орлова Н.Н", "1"};
        editingDataBase.updateElementString(tableName, columnName, values);
        conAndClos.close();
    }

    @Test
    public void updateElementLongTest() throws SQLException{
        conAndClos.connect();
        String tableName = "личные данные";
        String columnName = "ИНН";
        long [] values = {123451555908L, 1};
        editingDataBase.updateElementLong(tableName,columnName,values);
        conAndClos.close();
    }
}