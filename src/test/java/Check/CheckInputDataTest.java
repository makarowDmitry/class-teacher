package Check;

import org.junit.Test;

import static org.junit.Assert.*;

public class CheckInputDataTest {
    CheckInputData checkInputData = new CheckInputData();
    @Test
    public void checkTextTrue(){
        String text = "Макаров А.В";
        assertTrue(checkInputData.checkText(text));
    }

    @Test
    public void checkTextFalse(){
        String text = "Макаров23";
        assertFalse(checkInputData.checkText(text));
    }

    @Test
    public void checkNumberTrue(){
        assertTrue(checkInputData.checkNumber("234",3));
    }

    @Test
    public void checkNumberFalse(){
        assertFalse(checkInputData.checkNumber("23",3));
    }

    @Test
    public void checkAssessmentTrue(){
        assertTrue(checkInputData.checkAssessment("1"));
        assertTrue(checkInputData.checkAssessment("3"));
        assertTrue(checkInputData.checkAssessment("5"));
    }

    @Test
    public void checkAssessmentFalse(){
        assertFalse(checkInputData.checkAssessment("0"));
        assertFalse(checkInputData.checkAssessment("32"));
        assertFalse(checkInputData.checkAssessment("6"));
    }

    @Test
    public void checkPhoneTrue(){
        assertTrue(checkInputData.checkPhone("89123456789"));
    }

    @Test
    public void checkPhoneFalse(){
        assertFalse(checkInputData.checkPhone("79123435564"));
        assertFalse(checkInputData.checkPhone("87123435564"));
        assertFalse(checkInputData.checkPhone("9123435564"));
        assertFalse(checkInputData.checkPhone("891234355643"));
    }
}